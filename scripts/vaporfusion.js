/******************************************************************************
 ******************************** Initialisation ******************************
 *****************************************************************************/

let panier = JSON.parse(localStorage.getItem('panier')) || [];

window.addEventListener('load', function(){
    var panier = JSON.parse(localStorage.getItem('panier')) || [];
    calculerPrixTotal(panier);
    afficherPanierTemp(panier);
    return panier;
});

/******************************************************************************
 ******************************* Filtres Produits *****************************
 *****************************************************************************/

function filtrer(categorie) {
    var produits = document.querySelectorAll('.produit');

    produits.forEach(function(produit) {
        if (produit.classList.contains(categorie)) {
            produit.style.display = 'block';
        } else {
            produit.style.display = 'none';
        }
    });
}

function afficherTous() {
    var produits = document.querySelectorAll('.produit');

    produits.forEach(function(produit) {
        produit.style.display = 'block';
    });
}

/******************************************************************************
 ****************************** Reset Selecteurs ******************************
 *****************************************************************************/

function resetSelecteurs() {
    var selecteurs = document.querySelectorAll('select');
    selecteurs.forEach(function(selecteur) {
        selecteur.selectedIndex = 0;
    });
}


/******************************************************************************
 ********************************** Prix Produit ******************************
 *****************************************************************************/

function calculerPrix(selectElement) {
    var parentProduit = selectElement.closest('.produit');
    var prixElement = parentProduit.querySelector('#prixProduit');
    var volume = parseFloat(parentProduit.querySelector('.selectVolume').value);
    var nicotine = parseFloat(parentProduit.querySelector('.selectNicotine').value);
    var prixTotal = volume + nicotine;

    prixElement.textContent = 'Prix: ' + prixTotal.toFixed(2) + '€';
}

/******************************************************************************
 ***************************** Modification Panier ****************************
 *****************************************************************************/

function ajouterAuPanier(buttonElement) {
    var produit = buttonElement.parentElement;
    var nomProduit = produit.querySelector(".name").textContent;
    var prixProduitText = produit.querySelector("#prixProduit").textContent;
    var volumeProduit = produit.querySelector(".selectVolume option:checked").textContent;
    var nicotineProduit = produit.querySelector(".selectNicotine option:checked").textContent;
    var quantiteProduit = produit.querySelector(".selectQuantite option:checked").textContent;


    var prixProduit = parseFloat(prixProduitText.replace('Prix: ', '').replace('€', ''));

    var produitAjoute = {
        nom: nomProduit,
        prix: prixProduit,
        volume: volumeProduit,
        nicotine: nicotineProduit,
        quantite: quantiteProduit
    };

    ajouterProduitAuPanier(produitAjoute);
    sauvegarderPanier();
}

function ajouterProduitAuPanier(produit) {
    panier.push(produit);
    sauvegarderPanier();
    calculerPrixTotal(panier);
    resetSelecteurs();
    afficherPanierTemp(panier);
};

function calculerPrixTotal(panier) {
    var total = 0;

    panier.forEach(function(item) {
        if (item.prix !== undefined) {
            total += item.prix * item.quantite;
        }
    });

    var totalPanierElement = document.getElementById('totalPanier');
    totalPanierElement.textContent = 'Prix total : ' + total.toFixed(2) + '€';
}


function supprimerDuPanier(index) {
    if (index < 0 || index >= panier.length) {
        console.error("Index invalide.");
        return;
    }
    
    panier.splice(index, 1);

    calculerPrixTotal(panier);
    afficherPanier(panier);
    sauvegarderPanier();
}

/******************************************************************************
 ****************************** Affichage Panier ******************************
 *****************************************************************************/

function afficherPanier(panier) {
    var listePanier = document.getElementById('listeArticles');
    listePanier.innerHTML = '';

    panier.forEach(function(item, index) {
        var listItem = document.createElement('li');
        listItem.textContent = item.nom + ' - ' + item.volume + ' - ' + item.nicotine + ' - ' + item.quantite;

        if (item.prix !== undefined) {
            listItem.textContent += ' - Prix: ' + item.prix.toFixed(2) + '€    ';
        }

        var deleteButton = document.createElement('button');
        deleteButton.textContent = 'Supprimer';
        deleteButton.className = 'delete';

        deleteButton.onclick = function() {
            supprimerDuPanier(index);
        };

        listePanier.appendChild(listItem);
        listItem.appendChild(deleteButton);
    });

    document.getElementById('panierPopup').classList.remove('hidden');
}

function afficherPanierTemp(panier) {
    var panierPopup = document.getElementById('panierPopup');
    afficherPanier(panier);

    setTimeout(function() {
        panierPopup.classList.add('hidden')
    }, 2000);
}

function fermerPanier() {
    document.getElementById("panierPopup").classList.add("hidden");
}

function sauvegarderPanier() {
    localStorage.setItem('panier', JSON.stringify(panier));
}

function reinitialiserPanier() {
    panier = [];
    afficherPanier(panier);
}

/******************************************************************************
 ********************************** Commande mail *****************************
 *****************************************************************************/

function envoyerPanierParEmail() {
    var contenuPanier = "";
    var totalPanier = document.getElementById("totalPanier").textContent;

    var listeArticles = document.getElementById("listeArticles").children;

    for (var i = 0; i < listeArticles.length; i++) {
        var article = listeArticles[i].cloneNode(true);
        var supprimerBouton = article.querySelector('button');
        if (supprimerBouton) {
            supprimerBouton.remove();
        }
        var texteArticle = article.textContent.trim();
        contenuPanier += texteArticle + "\n";
    }

    var corpsEmail = "Contenu du Panier:\n\n" + contenuPanier + "\n" + totalPanier;

    var email = "vapetechfix@gmail.com";
    var sujet = "Repair'Ordi - Commande";
    var lienEmail = "mailto:" + encodeURIComponent(email) + "?subject=" + encodeURIComponent(sujet) + "&body=" + encodeURIComponent(corpsEmail);

    window.location.href = lienEmail;

    reinitialiserPanier();
    resetSelecteurs();
    calculerPrixTotal(panier);
}